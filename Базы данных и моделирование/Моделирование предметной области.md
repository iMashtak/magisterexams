# Понятие предметной области. Концептуальное моделирование предметных областей. Основные идеи и понятия. Модель «сущность-связь-свойства» (ER-модель). Представление отношения одиночной и множественной подтипизации. Представление отношения агрегации.

## Определения

**Предметная область** — часть реального мира, рассматриваемая в пределах данного контекста. Под контекстом здесь может пониматься, например, область исследования или область, которая является объектом некоторой деятельности.

*Модель предметной области* - это представление объектов реального мира в терминах предметной области.

*Концептуальная модель* — это модель, представленная множеством понятий и связей между ними, определяющих смысловую структуру рассматриваемой предметной области или её конкретного объекта.

**Концептуальная модель** — модель предметной области, состоящей из перечня взаимосвязанных понятий, используемых для описания этой области, вместе со свойствами и характеристиками, классификацией этих понятий, по типам, ситуациям, признакам в данной области и законов протекания процессов в ней.

*Моделирование предметной области* - один из начальных этапов проектирования информационной системы, необходимый для выявления, классификации и формализации сведений обо всех аспектах предметной области, определяющих свойства разрабатываемой системы. Такой тип использования моделей - один из самых важных, например, потому что так используются модели, которые получаются в результате анализа предметной области. Концептуальные модели довольно стабильны: если не меняется предметная область, то нет нужды  менять и модель.

**Модель Сущность-Связь (ER-модель)** — это модель данных, позволяющая описывать концептуальные схемы. Она предоставляет графическую нотацию, основанную на блоках и соединяющих их линиях, с помощью которых можно описывать объекты и отношения между ними какой-либо другой модели данных. В этом смысле ER-модель является средством описания моделей данных. ER-модель удобна при прототипировании (проектировании) информационных систем, баз данных, архитектур компьютерных приложений, и других систем (далее, моделей). С её помощью можно выделить ключевые сущности, присутствующие в модели, и обозначить отношения, которые могут устанавливаться между этими сущностями. 

**Сущность** – это абстрактный объект, который в конкретном контексте имеет независимое существование. **Отношение** – это ассоциация сущностей. **Атрибут** – это свойство сущности или связи. 

Атрибуты используются не только для описания свойств сущностей и
связей, но и для идентификации их экземпляров. Потенциальным **ключом** называется подмножество атрибутов (типа сущности или связи), которое функционально полно определяет значение любого атрибута. Т.е. ключ однозначно определяет экземпляр сущности. Основными свойствами потенциального ключа являются:

*  уникальность – не может быть двух и более экземпляров сущности
с одинаковыми значениями ключа;
* неизбыточность – никакое подмножество ключа не может выступать
в роли ключа;
* обязательность (определенность) – ни при каких условиях атрибуты
ключа не могут принимать неопределенные (Null) значения. 

Вводятся дополнительные понятия, связанные со свойствами отношений:

* Определяется понятие *входимости* сущности в отношение. Это понятие говорит о том, в какой пропорции могут находиться сущности, соединяемые некоторым отношением. Входимость сущности в отношение определяется как числовой интервал. Этот интервал записывается над линией, соединяющей отношение и сущность.
* Различают *кардинальности* отношений: "один к одному", "один ко многим", "многие ко многим" и другие. В этом случае такого рода тип интерпретируется так: одной сущности $`A`$ соответствует множество сущностей $`B`$. Знак $`1`$ записывают около линии, соединяющей отношение с сущностью $`A`$, а знак $`N`$ --- около линии рядом с $`B`$.
* Отношение может связывать несколько сущностей. Количество сущностей, связываемых отношением, называется *степенью* отношения.

**Обобщением** называется абстракция, которая позволяет трактовать класс различных подобных объектов (сущностей) как один поименованный обобщенный тип объекта. В результате обобщения между объектами устанавливается отношение (связь) с семантикой «есть». При помощи обобщения достигается подтипизация.

**Агрегацией** называется абстракция, которая позволяет рассматривать
связь (отношение) между объектами как новый объект. В результате агрегации между объектами устанавливается отношение (связь) с семантикой «есть часть». В ER-модели сущность представляет собой агрегат (отношение) атрибутов.

---