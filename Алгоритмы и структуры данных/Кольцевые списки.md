# Односвязные и двусвязные кольцевые списки. Структуры данных. Алгоритмы поиска, вставки и удаления элементов в односвязных и двусвязных кольцевых списках. Алгоритмическая сложность.

**Связный список** --- структура данных, состоящая из узлов, каждый из которых содержит данные и ссылки на следующие узлы списка. Если ссылка одна, то она указывает на следующий элемент списка, а сам список называется **односвязным**. Если ссылок две, то одна указывает на следующий элемент, а другая --- на предыдущий, а сам список называется **двусвязным**. Если у последнего элемента списка ссылка на следующий элемент пустая, то это **линейный** список, если же она указывает на первый элемент списка, то это **кольцевой** список. Аналогично для двусвязных.

Все последующие моменты поясним в виде python-кода.

**Двусвязный кольцевой список.** Реализация при помощи элемента-разделителя.

Добавление: `append`, `prepend`, `add`. Поиск: `get`.

```python
class Node:
  def __init__(self, data):
    self.data = data
    self.next = None
    self.prev = None

  def is_delimeter(self):
    return self.data == None

class List:  
  head: Node
  length: int

  def __init__(self):
    self.head = Node(None)
    self.head.next = self.head
    self.head.prev = self.head
    self.length = 0

  def is_empty(self):
    return self.head.next == None

  # O(1)
  def prepend(self, data):
    item = Node(data)
    item.next = self.head.next
    item.prev = self.head
    if self.is_empty():
      self.head.prev = item
    self.head.next.prev = item
    self.head.next = item
    self.length += 1

  # O(1)
  def append(self, data):
    item = Node(data)
    item.next = self.head
    item.prev = self.head.prev
    if self.is_empty():
      self.head.next = item
    self.head.prev.next = item
    self.head.prev = item
    self.length += 1

  # O(n)
  def get(self, pos: int) -> Node:
    if pos > self.length or self.is_empty():
      raise Exception()
    item = self.head.next
    while not pos == 0:
      item = item.next
      pos -= 1
    return item

  # O(n)
  def add(self, data, pos: int):
    if pos == 0:
      self.prepend(data)
      return
    if pos == self.length:
      self.append(data)
      return
    target = self.get(pos)
    item = Node(data)
    item.next = target
    item.prev = target.prev
    target.prev.next = item
    target.prev = item
    self.length += 1
```

Аналогично удаление состоит в том, чтобы найти через `get` удаляемый элемент и переприсвоить значения полей `next` и `prev` у обоих его соседей.

Алгоритм поиска можно было бы слегка оптимизировать путём проверки, откуда ближе идти до `pos` - с конца списка или с начала. То есть, если `pos < length / 2`, то надо идти от начала списка через ссылки на `next`. Иначе через `prev`. Данный код был опущен для краткости.

---