# Понятие процесса в операционной системе. Управление процессами. Механизмы управления памятью. Понятие потока. Механизмы взаимодействия: сигналы, семафоры. Конкуренция за ресурсы. Взаимные блокировки.

В этой модели все выполняемое на компьютере программное обеспечение, иногда включая операционную систему, сведено к ряду последовательных процессов, или, для краткости, просто процессов. **Процесс** — это просто экземпляр выполняемой программы, включая текущие значения счетчика команд, регистров и переменных. Концептуально у каждого процесса есть свой, виртуальный, центральный процессор. Разумеется, на самом деле настоящий центральный процессор постоянно переключается между процессами, но чтобы понять систему, куда проще думать о наборе процессов, запущенных в (псевдо) параллельном режиме, чем пытаться отслеживать, как центральный процессор переключается между программами. Это постоянное переключение между процессами, как мы уяснили в главе 1, называется мультипрограммированием, или многозадачным режимом работы.

Существуют четыре основных события, приводящих к созданию процессов.

1. Инициализация системы.
2. Выполнение работающим процессом системного вызова, предназначенного для создания процесса.
3. Запрос пользователя на создание нового процесса.
4. Инициация пакетного задания.

После создания процесса родительский и дочерний процессы обладают своими собственными, отдельными адресными пространствами. 

В некоторых системах, когда процесс порождает другой процесс, родительский и дочерний процессы продолжают оставаться определенным образом связанными друг с другом. Дочерний процесс может и сам создать какие-нибудь процессы, формируя иерархию процессов.

В качестве другого примера, поясняющего ту ключевую роль, которую играет иерархия процессов, давайте рассмотрим, как UNIX инициализирует саму себя при запуске сразу же после начальной загрузки компьютера. В загрузочном образе присутствует специальный процесс, называемый init. В начале своей работы он считывает файл, сообщающий о количестве терминалов. Затем он разветвляется, порождая по одному процессу на каждый терминал. Эти процессы ждут, пока кто-нибудь не зарегистрируется в системе. Если регистрация проходит успешно, процесс регистрации порождает оболочку для приема команд. Эти команды могут породить другие процессы и т.д. Таким образом, все процессы во всей системе принадлежат единому дереву, в корне которого находится процесс init.

Процесс может находиться в 3х состояниях:

* выполняемый (в данный момент использующий центральный процессор)
* готовый (работоспособный, но временно приостановленный, чтобы дать возможность выполнения другому процессу)
* заблокированный (неспособный выполняться, пока не возникнет какое-нибудь внешнее событие). Например, заблокирован ожиданием пользовательского ввода

Для реализации модели процессов операционная система ведет таблицу (состоящую из массива структур), называемую таблицей процессов, в которой каждая запись соответствует какому-нибудь процессу.

**Потоки** --- похожие на процессы сущности, но у них общая память, общее адресное пространство. Каждый процесс имеет поток, в котором он выполняется.

Элементы, присущие каждому процессу:

* адресное пространство
* глобальные переменные
* открытые файлы
* дочерние процессы
* необработанные аварийные сигналы
* сигналы и обработчики сигналов

Элементы, присущие каждому потоку:

* счётчик команд
* регистры
* стек
* состояние

Что не присуще потоку, то он разделяет с другими потоками. С помощью потоков мы пытаемся достичь возможности выполнения нескольких функций, использующих набор общих ресурсов с целью тесного сотрудничества при реализации какой-нибудь задачи.

Состязательная ситуация (состояние гонки) позникает при попытке нескольких потоков одновременно записывать данные в одну ячейку памяти. Такое может происходит, так как операции проверки, свободен ли ресурс, и обращения к ресурсу могут быть выполнены не последовательно в потоке. Может случиться так, что первый проток проверит доступность ресурса, затем исполнение переключится на другой поток, который займёт ресурс, а потом вернётся к первому, который не знает, что ресурс уже занят. Взаимная блокировка (deadlock) — ситуация, при которой несколько потоков находятся в состоянии ожидания ресурсов, занятых друг другом, и ни один из них не может продолжать свое выполнение.

Механизмы взаимодействия:

1. Команда `TSL RX,LOCK` (Test and Set Lock). Она считывает содержимое слова памяти lock в регистр RX, а по адресу памяти, отведенному для lock, записывает ненулевое значение. При этом гарантируются неделимость операций чтения слова и сохранение в нем нового значения — никакой другой процесс не может получить доступ к слову в памяти, пока команда не
завершит свою работу.
1. Семафор. Это отдельный тип переменной, специально предназначенный для синхронизации. И проверка значения, и его изменение, и, возможно, приостановка процесса осуществляются как единое и неделимое атомарное действие. Это можно осуществить при помощи `TSL` или аналогов. Семафор --- целочисленная переменная, хранящая количество потоков, ждущих критическую область памяти в своё пользование.
1. Мьютекс — это совместно используемая переменная, которая может находиться в одном из двух состояний: заблокированном или незаблокированном. Следовательно, для их представления нужен только один бит, но на практике зачастую используется целое число, при этом нуль означает незаблокированное, а все остальные значения — заблокированное состояние. Мьютекс --- двоичный семафор, упрощённый.
1. Сигнал (Unix) — асинхронное уведомление процесса о каком-либо событии, один из основных способов взаимодействия между процессами. Когда сигнал послан процессу, операционная система прерывает выполнение процесса, при этом, если процесс установил собственный обработчик сигнала, операционная система запускает этот обработчик, передав ему информацию о сигнале, если процесс не установил обработчик, то выполняется обработчик по умолчанию.

## Механизмы управления памятью

**Адресное пространство** — это набор адресов, который может быть использован процессом для обращения к памяти. У каждого процесса имеется собственное адресное пространство, независимое от того адресного пространства, которое принадлежит другим процессам.

*Управление памятью с помощью битовых матриц*. При использовании битовых матриц память делится на единичные блоки размером от нескольких слов до нескольких килобайт. С каждым единичным блоком соотносится один бит в битовой матрице, который содержит 0, если единичный блок свободен, и 1, если он занят (или наоборот). Основная проблема заключается в том, что при решении поместить в память процесс, занимающий k единичных блоков, диспетчер памяти должен искать в битовой матрице непрерывную последовательность нулевых битов. Поиск в битовой матрице последовательности заданной длины — довольно медленная операция (поскольку последовательность может пересекать границы слов в матрице), и это обстоятельство служит аргументом против применения битовых матриц.

*Управление памятью с помощью связанных списков*. Другим способом отслеживания памяти является ведение связанных списков распределенных и свободных сегментов памяти, где сегмент либо содержит процесс, либо является пустым пространством между двумя процессами. Каждая запись в списке хранит обозначение, содержит сегмент «дыру» — hole (H) или процесс — process (P), адрес, с которого сегмент начинается, его длину и указатель на следующую запись.

В основе **виртуальной памяти** лежит идея, что у каждой программы имеется собственное адресное пространство, которое разбивается на участки, называемые страницами. Каждая страница представляет собой непрерывный диапазон адресов. Эти страницы отображаются на физическую память, но для запуска программы одновременное присутствие в памяти всех страниц необязательно. Когда программа ссылается на часть своего адресного пространства, находящегося в физической памяти, аппаратное обеспечение осуществляет необходимое отображение на лету.

---