# coding=utf-8

# 0. Imports paths from readme
# 1. Collect all to one object
# 2. Convert some places manually
# 3. Write to md
# 4. Call pandoc md->tex
# 5. Fix some places manually
# 6. Call XeLaTeX to build

import sys
import re
import os
import subprocess

class Question:
    def __init__(self, header, content):
        self.header = header
        self.content = content

class QuestionTheme:
    def __init__(self, header):
        self.questions = []
        self.header = header

class QuestionBlock:
    def __init__(self, number):
        self.themes = []
        self.number = number

class Document:
    def __init__(self):
        self.blocks = []


def get_markdown_content(file_path):
    content = []
    with open(file_path, "r") as md_file:
        content = md_file.readlines()
    if len(content) == 0:
        return ("", [])
    header = content[0].lstrip("# ").replace("\n", "").replace("\r", "")
    stop_index = len(content)
    for i in range(0, len(content)):
        if content[i].startswith("---"):
            stop_index = i
    return (header, content[2:stop_index])

def fix_markdown_for_pandoc(content):
    fixed_content = []
    is_math_env = False
    for line in content:
        fixed = line
        fixed = line.replace("$`", "$").replace("`$", "$")
        fixed = re.sub("\[\^.*\]", "", fixed)
        if re.search(r"```math$", fixed):
            is_math_env = True
            fixed = fixed.replace("```math", "\\begin{equation*}")
        if re.search(r"```$", fixed) and is_math_env:
            fixed = fixed.replace("```", "\\end{equation*}")
            is_math_env = False
        fixed_content.append(fixed)
    return fixed_content

def collect_questions_to_document():
    readme_content = []
    with open("./README.md", "r") as readme_file:
        readme_content = readme_file.readlines()
    document = Document()
    c_block = QuestionBlock(0)
    c_theme = QuestionTheme("")
    for line in readme_content:
        is_block = line.startswith("## ")
        is_theme = line.startswith("### [")
        is_question = re.search(r"1\. :.*: \[", line)
        if is_block:
            c_block = QuestionBlock(c_block.number + 1)
            document.blocks.append(c_block)
        if is_theme:
            header = re.sub(r"\].*", "", line).replace("### [", "").replace("\n", "").replace("\r", "")
            c_theme = QuestionTheme(header)
            c_block.themes.append(c_theme)
        if is_question:
            file_path = re.search(r"\]\(.*\)", line).group().replace("%20", " ").lstrip("](").rstrip(")").replace("\n", "").replace("\r", "")
            header, content_lines = get_markdown_content(file_path)
            content_lines = fix_markdown_for_pandoc(content_lines)
            question = Question(header, "".join(content_lines))
            c_theme.questions.append(question)
    return document

def print_document_to_md_file():
    document = collect_questions_to_document()
    with open("build/document.md", "w") as md_file:
        for block in document.blocks:
            print(block.number)
            for theme in block.themes:
                print("    ", theme.header)
                for question in theme.questions:
                    print("        ", question.header[:50], "...")
                    md_file.write("# {}\n\n".format(question.header))
                    md_file.write("> **Тема:** {}\n\n".format(theme.header))
                    md_file.write(question.content)

def edit_generated_tex(tex_path):
    tex_content = []
    with open(tex_path, "r") as tex_file:
        tex_content = tex_file.readlines()
    for i in range(0, len(tex_content)):
        line = tex_content[i]
        line = line.replace("\\section", "\\chapter")
        line = line.replace("\\subsection", "\\section*")
        tex_content[i] = line
    with open(tex_path, "w") as tex_file:
        tex_file.write("".join(tex_content))

template = """
\\documentclass[12pt,a4paper,oneside,final]{report}
\\input{styles}
\\usepackage{title}
\\begin{document}
    \\maketitle
    \\tableofcontents
    \\newpage
    \\input{doc}
\\end{document}
"""

def call_pandoc_md_to_tex():
    print_document_to_md_file()

    os.chdir("build")
    os.system("pandoc document.md -o doc.tex")
    edit_generated_tex("doc.tex")
    with open("template.tex", "w") as template_file:
        template_file.write(template)
    os.system("latexmk -xelatex -synctex=1 -interaction=nonstopmode -file-line-error -outdir=.build template.tex")

call_pandoc_md_to_tex()