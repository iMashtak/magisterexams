# Маршруты и расстояния в графах. Волновой алгоритм определения расстояния в графе. Радиус и диаметр графа. Алгоритм нахождения расстояния во взвешенном графе. Расстояния в ориентированном графе.

**Маршрутом** в графе $`G=\langle V,U \rangle`$ называется чередующаяся последовательность вершин и рёбер $`v_1,u_1,v_2,u_2,\dots,v_k`$. Последовательность начинается и заканчивается вершиной. *Путь* --- маршрут, в котором нет повторяющихся вершин. Замкнутый путь называется *контуром*. *Цепь* --- маршрут, в котором нет повторяющихся рёбер. Цепь называется *простой*, если в ней нет повторяющихся вершин. Замкнутая цепь --- *цикл*. Замкнутая цепь, в которой не повторяются вершины, кроме первой и последней, называется *простым циклом*.

*Длиной маршрута* называется количество рёбер в нём. **Расстоянием** $`d(v_i, v_j)`$ между двумя вершинами называется длина кратчайшей простой цепи, соединяющей эти вершины. Если такой цепи не существует, то расстояние принимается бесконечным.

**Диаметром** $`D(G)`$ связного графа называется максимально возможное расстояние между двумя его вершинами. *Центром* графа называется такая вершина, что максимальное расстояние между ней и любой другой вершиной является наименьшим из всех возможных. Это расстояние называется **радиусом** $`R(G)`$.

```math
\begin{matrix}
D(G) = \max\limits_{v_i,v_j \in V}(d(v_i,v_j))
&
R(G) = \min\limits_{v_i \in V}\max\limits_{v_j \in V} (d(v_i,v_j))
\end{matrix}
```

Свойство диаметра графа: $`R(G) \le D(G) \le 2R(G)`$

**Взвешенный граф** --- это граф, рёбрам которого присваивается числовая характеристика: вес. Вес учитывается при расчёте расстояния между вершинами.

***Волновой алгоритм определения расстояния в графе.***

Каждой вершине сопоставим метку --- минимальное известное расстояние от этой вершины до вершины $`v`$, относительно которой ищутся расстояния.

Метка самой вершины $`a`$ полагается равной $`0`$, метки остальных --- бесконечности. Все вершины графа помечаются как непосещённые.

Если все вершины посещены, алгоритм завершается. Иначе из ещё не посещённых вершин выбирается вершина $`v'`$, имеющая минимальную метку. Для каждого непосещённого соседа этой вершины вычислим длину пути как сумму метки вершины $`v'`$ и веса ребра (если есть). Если получившееся число меньше метки рассматриваемого соседа, то присваиваем эту соседу вычисленное число. Рассмотрев всех соседей, помечаем вершину $`v'`$ как посещённую, после чего повторяем шаг алгоритма.

В ориентированных графах может быть такое, что $`d(v_i, v_j) \ne d(v_j, v_i)`$.

---